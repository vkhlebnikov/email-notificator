package com.example.emailnotificator.web.rest;

import com.example.emailnotificator.service.NotificatorService;
import com.example.emailnotificator.service.model.EmailNotification;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class NotificatorController {

    private final NotificatorService notificatorService;

    @GetMapping("/")
    public String emailMessageForm(Model model) {
        model.addAttribute("emailNotification", new EmailNotification());
        return "email-notification-form";
    }

    @PostMapping("/notify")
    public String emailMessageSubmit(@Validated @ModelAttribute EmailNotification emailNotification, Model model) {
        notificatorService.notify(emailNotification);
        model.addAttribute("emailNotification", emailNotification);
        return "email-notification-result";
    }
}
