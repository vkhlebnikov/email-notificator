package com.example.emailnotificator.service;

import com.example.emailnotificator.service.model.EmailNotification;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificatorServiceImpl implements NotificatorService {

    private final JavaMailSender javaMailSender;

    @Override
    public void notify(EmailNotification emailNotification) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(emailNotification.getEmail());
        simpleMailMessage.setSubject(emailNotification.getSubject());
        simpleMailMessage.setText(emailNotification.getText());
        javaMailSender.send(simpleMailMessage);
    }
}
