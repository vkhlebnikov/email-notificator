package com.example.emailnotificator.service;

import com.example.emailnotificator.service.model.EmailNotification;

public interface NotificatorService {
    void notify(EmailNotification emailNotification);
}
