package com.example.emailnotificator.service.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailNotification {
    @NotEmpty
    @Email
    private String email;
    @NotEmpty
    private String subject;
    @NotEmpty
    private String text;
}
